//
//  HomeController.swift
//  SentimentAnalyser
//
//  Created by Nessim Halfon on 16/12/2019.
//  Copyright © 2019 Nessim Halfon. All rights reserved.
//

import UIKit
import CircleMenu
import SafariServices

class HomeController: UIViewController {

    //MARK:- IBOutlet
    @IBOutlet weak var circleButton: CircleMenu!
    let service = Service()
    
    //MARK:- Properties
    let standards = UserDefaults.standard
    
    let items: [(icon: String, color: UIColor)] = [ ("search", UIColor.MyTheme.bordeau),("twitter", UIColor.MyTheme.bordeau), ("pencil", UIColor.MyTheme.bordeau), ("stars", UIColor.MyTheme.bordeau) ]
    
    //MARK:- Lyfe Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupMenu()
    }
    
    //MARK:- Setup Func
    
    /// Setup le CircleMenu
    private func setupMenu() {
        circleButton.delegate = self
        circleButton.layer.cornerRadius = circleButton.frame.size.width / 2
        circleButton.layer.borderWidth = 2
        circleButton.layer.borderColor = UIColor.black.cgColor
    }
    
    //MARK:- Func
    
    /// Check if an User is Logged.
    /// Return un Bool.
    private func isLoggedIn() -> Bool {
        guard standards.bool(forKey: "isLog") else { return false }
        return true
    }
    
    /// Save les infos de l'User dans les UserDefaults.
    /// - parameter user : L'User.
    /// - parameter username: L'username de l'User.
    /// - parameter imageUrl : L'imageUrl de l'User.
    /// - parameter created : La date de création du compte de l'User.
    private func save(user: User, username: String, imageUrl: String, created: String) {
       standards.set(user.id, forKey: "id")
       standards.set(user.screenname, forKey: "screenname")
       standards.set(user.isLog, forKey: "isLog")
       standards.set(username, forKey: "username")
       standards.set(imageUrl, forKey: "imageUrl")
       standards.set(created, forKey: "created")
    }
    
    //MARK:- Fetch Func
    
    /// Recupere les infos de l'user et rediriger vers la page de profil.
    private func fetchUser() {
        service.login(vc: self) { result in
            switch result {
            case .success(value: let user):
                
                self.service.fetchUserInfo(user.screenname) { result in
                    switch result {
                    case .success(value: let json):
                        if let username = json["name"].string,
                            let imageUrl = json["profile_image_url_https"].string,
                            let created = json["created_at"].string {

                            self.save(user: user, username: username, imageUrl: imageUrl, created: created)
                        }

                        self.showLoader()
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                            self.hideLoader(view: self.view)
                            self.show(.MyTweets, identifier: .MyTweets)
                        }
                        
                    case .failure(error: let error):
                        print(error)
                    }
                }
            case .failure(error: let error):
                print(error)
            }
        }
    }
    
    
    /// Permet de se loger si on est pas déjà connecter sinon envoi sur la page de Twitter.
    private func login() {
        switch isLoggedIn() {
            case true:
                show(.MyTweets, identifier: .MyTweets)
            case false:
                fetchUser()
        }
    }
    
}

//MARK:-
extension HomeController: CircleMenuDelegate {
    
    /// Design le CircleButton
    func circleMenu(_ circleMenu: CircleMenu, willDisplay button: UIButton, atIndex: Int) {
        
        button.layer.borderWidth = 2
        button.layer.borderColor = UIColor.black.cgColor

        button.backgroundColor = items[atIndex].color
        button.setImage(UIImage(named: items[atIndex].icon), for: .normal)

        // set highlited image
        let highlightedImage = UIImage(named: items[atIndex].icon)?.withRenderingMode(.alwaysTemplate)
        button.setImage(highlightedImage, for: .highlighted)
        button.tintColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
        
    }

    /// Apres l'animation appel en fonction de l'index
    func circleMenu(_ circleMenu: CircleMenu, buttonDidSelected button: UIButton, atIndex: Int) {
        
        switch atIndex {
            case 0:
                show(.SearchTwitter, identifier: .SearchTwitter)
            case 1:
                login()
            case 2:
                show(.Text, identifier: .Text)
            case 3:
                show(.VIP, identifier: .VIP)
            default: break
        }
    }
}

//MARK:-
extension HomeController: SFSafariViewControllerDelegate {
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}
