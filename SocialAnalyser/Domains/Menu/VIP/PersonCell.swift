//
//  PersonCell.swift
//  SentimentAnalyser
//
//  Created by Nessim Halfon on 26/04/2020.
//  Copyright © 2020 Nessim Halfon. All rights reserved.
//

import UIKit

class PersonCell: UICollectionViewCell {
    
    //MARK:- Static Properties
    static let identifier = "PersonCell"
    
    //MARK:- IBOutlet
    @IBOutlet weak var personImageView: UIImageView!
    
    //MARK:- Func
    
    /// Permet de setup la cell.
    /// - parameter image : L'image de profil de l'User.
    func setup(image: String?) {
        guard let image = image else { return }
        personImageView.image = UIImage(named: image)
        personImageView.layer.cornerRadius = personImageView.frame.size.width / 2
    }
    
    /// Permet de faire une animation de `bounce`.
    /// - parameter bounce : Bool permettant de faire l'animation ou non.
    func bounce(_ bounce: Bool) {
        UIView.animate(withDuration: 0.8, delay: 0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0.8, options: [.allowUserInteraction, .beginFromCurrentState], animations: {
            self.transform = bounce ? CGAffineTransform(scaleX: 0.9, y: 0.9) : .identity
        })
    }
}
