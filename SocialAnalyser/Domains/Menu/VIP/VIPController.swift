//
//  VIPController.swift
//  SentimentAnalyser
//
//  Created by Nessim Halfon on 23/04/2020.
//  Copyright © 2020 Nessim Halfon. All rights reserved.
//

import UIKit

class VIPController: UIViewController {

    //MARK:- IBOutlet
    @IBOutlet weak var vipCollectionView: UICollectionView!
    @IBOutlet weak var teamCollectionView: UICollectionView!
    @IBOutlet weak var companyCollectionView: UICollectionView!
    
    //MARK:- Properties
    let service = Service()
    var vips: [User] = []
    var teams: [User] = []
    var companys: [User] = []
    var text: String?
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        createData()
    }
    
    //MARK:- func
    
    /// Setup permettant d'avoir les données pour les TableViews.
    private func createData() {
        vips = User.createVIP()
        teams = User.createTeam()
        companys = User.createCompany()
    }
    
    /// Permet de récupérer les tweets d'une personne.
    /// - parameter screenName : Le screenName de l'User.
    /// - parameter username : L'username de l'User.
    private func fetchTimeLine(for screenName: String, username: String) {
        var tweets: [String] = []
        service.fetchTimeLine(for: screenName) { result in
            switch result {
                case .success(value: let jsons):
                    for json in jsons {
                        if json["full_text"].string!.count > 10 {
                            if let tweet = json["full_text"].string {
                                if !tweet.contains("RT @") {
                                    tweets.append(tweet.lematize())
                                }
                            }
                        }
                    }
                self.text = tweets.joined(separator: ". ")
                
                guard let requestController = self.storyboardViewController(name: .Result, identifier: .Request) as? RequestController else { return }
                
                requestController.text = self.text
                requestController.username = username
                self.navigationController?.pushViewController(requestController, animated: true)

            case .failure(_):
                self.displayAlert(title: "Impossible !", message: "\(username) à mis son compte Twitter en privé. \nVous ne pouvez donc pas consulter son profil.")
            }
        }
    }
    
    /// Permet de se diriger vers la page de recherche des tweets.
    private func goToSearchTweets(for screenName: String) {
        guard let tweetsController = storyboardViewController(name: .SearchTwitter, identifier: .Tweets) as? TweetsController else { return }
        tweetsController.text = screenName
        navigationController?.pushViewController(tweetsController, animated: true)
    }
}

//MARK:-
extension VIPController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == vipCollectionView {
            return vips.count
        } else if collectionView == teamCollectionView {
            return teams.count
        } else {
            return companys.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PersonCell.identifier, for: indexPath) as? PersonCell else { return UICollectionViewCell() }

        if collectionView == vipCollectionView {
            let vip = vips[indexPath.row]
            cell.setup(image: vip.imageUrl)
        } else if collectionView == teamCollectionView {
            let team = teams[indexPath.row]
            cell.setup(image: team.imageUrl)
        } else {
            let company = companys[indexPath.row]
            cell.setup(image: company.imageUrl)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == vipCollectionView {
            let vip = vips[indexPath.row]
            fetchTimeLine(for: vip.screenname, username: vip.username)
        } else if collectionView == teamCollectionView {
            let team = teams[indexPath.row]
            fetchTimeLine(for: team.screenname, username: team.username)
        } else {
            let company = companys[indexPath.row]
            goToSearchTweets(for: company.screenname)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100.0, height: 100.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? PersonCell {
            cell.bounce(true)
            cell.isUserInteractionEnabled = false
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? PersonCell {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                cell.bounce(false)
                cell.isUserInteractionEnabled = true
            }
        }
    }
}

