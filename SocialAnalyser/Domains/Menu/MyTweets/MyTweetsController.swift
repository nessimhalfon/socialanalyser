//
//  MyTweetsController.swift
//  SentimentAnalyser
//
//  Created by Nessim Halfon on 08/04/2020.
//  Copyright © 2020 Nessim Halfon. All rights reserved.
//

import UIKit

class MyTweetsController: UIViewController {

    //MARK:- IBOutlet
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var screennameLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var createdLabel: UILabel!
    
    //MARK:- Propeties
    let standards = UserDefaults.standard
    let service = Service()
    var user: User?
    var tweets: [String] = []
    var tweetsAnalyses: [String] = []
    var text: String?
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        read()
        setupView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        view.clipsToBounds = true
    }
    
    
    // MARK: - Func
    
    /// Setup la View.
    private func setupView() {
        guard let user = user else { return }
        setupLabel()
        setupImageView()
    
        fetchHome(user: user)
    }
    
    /// Setup les Labels.
    private func setupLabel() {
        guard let user = user else { return }
        usernameLabel.text = user.username
        screennameLabel.text = "@" + user.screenname
        idLabel.text = user.id
        createdLabel.text = "Compte crée le " + user.created.parseTwitterDate()
    }
    
    /// Setup l'ImageView
    private func setupImageView() {
        guard let user = user else { return }
        profileImageView.download(string: user.imageUrl?.imageFullSize)
        profileImageView.layer.cornerRadius = profileImageView.bounds.width / 2
    }
    
    //MARK:- Func
    
    /// Permet de récupérer les informations sauvegardé dans l'UserDefaults.
    private func read() {
        guard let id = standards.string(forKey: "id"),
            let screenname = standards.string(forKey: "screenname"),
            let username = standards.string(forKey: "username"),
            let imageUrl = standards.string(forKey: "imageUrl"),
            let created = standards.string(forKey: "created")
        else { return }
        
        let isLog = standards.bool(forKey: "isLog")
        
        createUser(id: id, screenname: screenname, isLog: isLog, username: username, imageUrl: imageUrl, created: created)
    }
    
    /// Permet de créer un User.
    /// - parameter id : L'id de l'User.
    /// - parameter screenname : Le screenname de l'User.
    /// - parameter isLog : Permet de savoir si l'utilisateur est connecter ou non.
    /// - parameter username : L'username de l'User.
    /// - parameter imageUrl : L'imageUrl de l'User.
    /// - parameter created : La date de création du compte de l'User.
    private func createUser(id: String, screenname: String, isLog: Bool, username: String, imageUrl: String, created: String) {
        user = User(id: id, screenname: screenname, isLog: isLog, username: username, imageUrl: imageUrl, created: created)
    }
    
    /// Permet de clear les informations sauvegardé dans l'UserDefaults
    private func clearData() {
        standards.removeObject(forKey: "id")
        standards.removeObject(forKey: "screenname")
        standards.removeObject(forKey: "username")
        standards.removeObject(forKey: "imageUrl")
        standards.removeObject(forKey: "created")
        standards.set(false, forKey: "isLog")
    }
    
    //MARK:- Fetch
    
    /// Permet de récupérer les tweets de l'User.
    /// - parameter user : L'User.
    private func fetchHome(user: User) {
        service.fetchTimeLine(for: user.screenname) { result in
            switch result {
             case .success(value: let jsons):
                for json in jsons {
                    if json["full_text"].string!.count > 10 {
                        if let tweet = json["full_text"].string {
                            self.tweetsAnalyses.append(tweet)
                            if !tweet.contains("RT @") {
                                self.tweets.append(tweet.lematize())
                            }
                        }
                    }
                }
                self.text = self.tweets.joined(separator: ". ")
                self.tableView.reloadData()
            case .failure(error: let error):
                print(error)
            }
        }
    }
    
    
    //MARK:- IBAction
    
    /// Action permettant de se déconnecter.
    @IBAction func logoutButton(_ sender: UIBarButtonItem) {
        clearData()
        navigationController?.popViewController(animated: true)
    }
    
    /// Action permettant d'analyser les tweets de l'User.
    @IBAction func analyseProfile(_ sender: CustomButton) {
        sender.pulse()
        guard let requestController = storyboardViewController(name: .Result, identifier: .Request) as? RequestController else { return }
               
        requestController.text = text
        requestController.username = "me"
        self.navigationController?.pushViewController(requestController, animated: true)
    }
}

//MARK:-
extension MyTweetsController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tweetsAnalyses.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: MyTweetsCell.identifier, for: indexPath) as? MyTweetsCell else { return UITableViewCell() }
        
        let tweet = tweetsAnalyses[indexPath.row]
        cell.setup(tweet: tweet)
        
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return UITableView.automaticDimension
        } else {
            return 50
        }
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return UITableView.automaticDimension
        } else {
            return 50
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView()
        footerView.backgroundColor = UIColor.clear
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
}
