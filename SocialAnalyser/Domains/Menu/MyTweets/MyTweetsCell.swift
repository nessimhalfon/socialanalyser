//
//  MyTweetsCell.swift
//  SentimentAnalyser
//
//  Created by Nessim Halfon on 05/05/2020.
//  Copyright © 2020 Nessim Halfon. All rights reserved.
//

import UIKit

class MyTweetsCell: UITableViewCell {

    //MARK:- Static Properties
    static let identifier = "MyTweetsCell"
    
    //MARK:- IBOutlet
    @IBOutlet weak var viewCell: UIView!
    @IBOutlet weak var tweetLabel: UILabel!
    
    //MARK:- Func
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        viewCell.setShadow()
    }
    
    /// Permet de setup la cell.
    /// - parameter tweet : Affiche le tweet.
    func setup(tweet: String) {
        viewCell.setRoundCorner(color: UIColor.MyTheme.bordeau, borderWidth: 1)
        tweetLabel.text = tweet
    }
}
