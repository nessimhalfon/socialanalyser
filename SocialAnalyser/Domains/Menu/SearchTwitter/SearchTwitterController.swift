//
//  SearchTwitterController.swift
//  SentimentAnalyser
//
//  Created by Nessim Halfon on 15/12/2019.
//  Copyright © 2019 Nessim Halfon. All rights reserved.
//

import UIKit

class SearchTwitterController: UIViewController {

    //MARK:- IBOutlet
    @IBOutlet weak var twitterTextfield: UITextField!
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
     
        setupView()
    }
    
    //MARK:- Func
    
    /// Setup la view.
    private func setupView() {
        twitterTextfield.delegate = self
        twitterTextfield.setShadow()
        twitterTextfield.layer.cornerRadius = 10
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeybord))
        view.addGestureRecognizer(tapGesture)
    }
    
    //MARK:- IBAction
    
    /// Lancer l'analyse des tweets.
    @IBAction func analyseButton(_ sender: CustomButton) {
        sender.pulse()
        guard let text = twitterTextfield.text, text.count > 0 else { return }
        performSegue(withIdentifier: "showTweets", sender: text)
        twitterTextfield.text = nil
    }
}

//MARK:-
extension SearchTwitterController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

//MARK:-
extension SearchTwitterController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showTweets",
            let controller = segue.destination as? TweetsController {
            guard let text = twitterTextfield.text, text.count > 0 else { return }
            controller.text = text
        }
    }
}
