//
//  TweetsController.swift
//  SentimentAnalyser
//
//  Created by Nessim Halfon on 16/12/2019.
//  Copyright © 2019 Nessim Halfon. All rights reserved.
//

import UIKit
import CoreML

class TweetsController: UIViewController {
    
    //MARK:- IBOutlet
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sentimentLabel: UILabel!
    @IBOutlet weak var separatedView: UIView!
    
    //MARK:- Properties
    let service = Service()
    var sentiTweet: Sentiment!
    var text: String?
    var tweets = [Tweet]()
    let tweetCount = 100
    
    var scorePositif = 0
    var scoreNegatif = 0
    var scoreNeutre = 0
    var scores: [Int] = []
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        sentiTweet = Sentiment()

    }
    
    //MARK:-
    
    /// Setup la view.
    private func setupView() {
        guard let text = text else { return }
        self.title = text
        fetchTweets(for: text)
    }
    
    /// Permet de mettre à jour le label en fonction du score de l'analyse.
    /// - parameter scorePositif : Le score des tweets positif.
    /// - parameter scoreNegatif : Le score des tweets négatif.
    /// - parameter scoreNeutre : Le score des tweets neutre.
    private func updateLabel(with scorePositif: Int, and scoreNegatif: Int, and scoreNeutre: Int) {
        
        guard let text = text else {  return }
        let maximum = max((max(scorePositif, scoreNegatif)), scoreNeutre)
        
        let analysePositif = "L'entreprise '\(text)' que vous avez analysé est ressenti Positivement par la communautée Twitter 👍🏻 \nAvec un score de \(scorePositif)% positif, \(scoreNegatif)% négatif et \(scoreNeutre)% neutre."
        let analyseNegatif = "L'entreprise '\(text)' que vous avez analysé est ressenti Négativement par la communautée Twitter 👎🏻\nAvec un score de \(scoreNegatif)%, \(scorePositif)% positif et \(scoreNeutre)% neutre."
        let analyseNeutre = "L'entreprise '\(text)' que vous avez analysé est ressenti Neutre par la communautée Twitter.\nAvec un score de \(scoreNeutre)%, \(scorePositif)% positif et \(scoreNegatif)% négatif."
        
        switch maximum {
        case scorePositif:
            self.sentimentLabel.attributedText = changeAttribute(forString: analysePositif, values: ["\(scorePositif)%", "Positivement"], color: UIColor.Sentiment.positif)
        case scoreNegatif:
            self.sentimentLabel.attributedText = changeAttribute(forString: analyseNegatif, values: ["\(scoreNegatif)%", "Négativement"], color: UIColor.Sentiment.negatif)
        case scoreNeutre:
            self.sentimentLabel.attributedText = changeAttribute(forString: analyseNeutre, values: ["\(scoreNeutre)%", "Neutre"], color: .gray)
        default: break
        }
    }
    
    /// Setup les labels.
    /// - parameter alpha : L'apha de la view pour la faire apparaitre/disparaitre.
    private func setupLabel(withAlpha alpha: CGFloat) {
        sentimentLabel.alpha = alpha
        separatedView.alpha = alpha
    }
    
    //MARK:- Fetch & Predict
    
    /// Permet de récupérer les tweets d'une personne/entreprise/hashtag/etc.
    /// - parameter text : La personne/entreprise/hashtag/etc.
    private func fetchTweets(for text: String) {
        setupLabel(withAlpha: 0.0)
        showLoader()
        service.fetch(text: text, lang: "en", count: tweetCount) { (result) in
            self.hideLoader(view: self.view)
            switch result {
            case .success(let result):
    
                for i in 0..<self.tweetCount {
                    if let tweet = result[i]["full_text"].string {
                        self.makePrediction(with: tweet)
                    }
                }
                self.setupLabel(withAlpha: 1.0)
                self.updateLabel(with: self.scorePositif, and: self.scoreNegatif, and: self.scoreNeutre)
            case .failure(let error):
                print("There was an error with the Twitter API Request : ", error)
            }
        }
        
    }
    
    /// Permet de faire une prédiction en fonction d'un tweet.
    /// Remplis les scores en fonction de la prédiction.
    /// Créer l'array pour la TableView, avec le design approprié.
    /// - parameter text : Le tweet à analyser.
    private func makePrediction(with text: String) {
        do {
            let prediction = try self.sentiTweet.prediction(text: text)
            let sentiment = prediction.label
            switch sentiment {
                case Type.Positive.rawValue:
                    scorePositif += 1
                    let tweet = Tweet(text: text, sentiment: sentiment, color: UIColor.Sentiment.positif)
                    self.tweets.append(tweet)
                    self.tableView.reloadData()
                case Type.Negative.rawValue:
                    scoreNegatif += 1
                    let tweet = Tweet(text: text, sentiment: sentiment, color: UIColor.Sentiment.negatif)
                    self.tweets.append(tweet)
                    self.tableView.reloadData()
                case Type.Neutral.rawValue:
                    scoreNeutre += 1
                    let tweet = Tweet(text: text, sentiment: sentiment, color: .gray)
                    self.tweets.append(tweet)
                    self.tableView.reloadData()
                default: break
            }
        } catch {
           print("There was an error with making the prediction : ", error)
       }
    }
    
    //MARK:- Func
    
    /// Permet de clear les données de la view pour faire une nouvelle analyse.
    private func clearData() {
        tweets = []
        tableView?.reloadData()
        scorePositif = 0
        scoreNegatif = 0
        scoreNeutre = 0
        sentimentLabel.text = nil
    }
    
    /// Permet de récupérer les tweets apres avoir remplis le TextField d'une Alert.
    private func fetchTweetFromAlert() {
        let alertController = UIAlertController(title: "", message: "", preferredStyle: .alert)
        alertController.addTextField { textField in
            textField.placeholder = "Review"
        }
        let confirmAction = UIAlertAction(title: "Sentiment analysis", style: .default) { [weak alertController] _ in
            guard let alertController = alertController,
                let textField = alertController.textFields?.first,
                let text = textField.text, text.count > 0 else { return }
                self.clearData()
                self.text = text
                self.title = text
                self.fetchTweets(for: text)
                self.updateLabel(with: self.scorePositif, and: self.scoreNegatif, and: self.scoreNeutre)
        }
        alertController.addAction(confirmAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
    
    //MARK:- IBAction
    
    /// Action permettant de récupérer les tweets.
    @IBAction func fetchTweetAction(_ sender: UIBarButtonItem) {
       fetchTweetFromAlert()
    }
}

//MARK:-
extension TweetsController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tweets.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TweetCell.identifier, for: indexPath) as? TweetCell else { return UITableViewCell() }
        
        let tweet = tweets[indexPath.section]
        print(tweet)
        cell.setup(sentiment: tweet.sentiment, tweet: tweet.text.lematize(), color: tweet.color)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView()
        footerView.backgroundColor = UIColor.clear
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 5
    }
}
