//
//  TweetCell.swift
//  SentimentAnalyser
//
//  Created by Nessim Halfon on 16/12/2019.
//  Copyright © 2019 Nessim Halfon. All rights reserved.
//

import UIKit

class TweetCell: UITableViewCell {

    //MARK:- Static
    static let identifier = "TweetCell"
    
    //MARK:- IBOutlet
    @IBOutlet weak var viewCell: UIView!
    @IBOutlet weak var sentimentLabel: UILabel!
    @IBOutlet weak var tweetLabel: UILabel!
    
    //MARK:- Func
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        viewCell.setShadow()
    }
    
    /// Permet de setup la cell.
    /// - parameter sentiment : Affiche le résultats de l'analyser (Positif/Négatif/Neutre).
    /// - parameter tweet : Affiche le tweet.
    /// - parameter color : Affiche la couleur en fonction du résultats de l'analyse.
    func setup(sentiment: String, tweet: String, color: UIColor) {
        viewCell.setRoundCorner(color: color)
        sentimentLabel.text = sentiment
        sentimentLabel.textColor = color
        tweetLabel.text = tweet
    }
    
}
