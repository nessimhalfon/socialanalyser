//
//  TextController.swift
//  SentimentAnalyser
//
//  Created by Nessim Halfon on 07/04/2020.
//  Copyright © 2020 Nessim Halfon. All rights reserved.
//

import UIKit

class TextController: UIViewController {

    //MARK:- IBOutler
    @IBOutlet weak var analyseButton: UIButton!
    @IBOutlet weak var predictTextView: UITextView!
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
    }
    
    //MARK:- Func
    
    /// Setup la view.
    private func setupView() {
        setupTextView()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeybord))
        view.addGestureRecognizer(tapGesture)
    }
    
    /// Setup le TextView.
    private func setupTextView() {
        predictTextView.delegate = self
        predictTextView.layer.borderWidth = 1
        predictTextView.layer.borderColor = UIColor.black.cgColor
        predictTextView.layer.cornerRadius = 20
        predictTextView.setShadow()
    }
    
    //MARK:- IBAction
    
    /// Action permettant de faire une prédiction du text.
    @IBAction func goToPredictButton(_ sender: CustomButton) {
        sender.pulse()
        guard let text = predictTextView.text, text.count > 0 else { return }
        
        guard let requestController = storyboardViewController(name: .Result, identifier: .Request) as? RequestController else { return }
        
        requestController.text = text
        self.navigationController?.pushViewController(requestController, animated: true)
        predictTextView.text = nil
        
    }
}

//MARK:-
extension TextController: UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}
