//
//  SplashScreenController.swift
//  SentimentAnalyser
//
//  Created by Nessim Halfon on 16/12/2019.
//  Copyright © 2019 Nessim Halfon. All rights reserved.
//

import UIKit
import Lottie

class SplashScreenController: UIViewController {
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        startAnimation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
//        showHome()
    }
    
    //MARK:- Func
    
    /// Lance l'animation du splashScreen.
    private func startAnimation() {
        
        let animationView = AnimationView(name: "loading")
        
        animationView.frame = CGRect(x: 0, y: 0, width: 400, height: 400)
        animationView.center = self.view.center
        animationView.contentMode = .scaleAspectFit
        view.addSubview(animationView)

        animationView.play { _ in
            self.showHome()
        }
    }
    
    /// Permet de rediriger vers le HomeController.
    private func showHome() {
        performSegue(withIdentifier: "showHome", sender: self)
    }
}
