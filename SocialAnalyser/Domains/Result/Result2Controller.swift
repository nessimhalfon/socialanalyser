//
//  Result2Controller.swift
//  SentimentAnalyser
//
//  Created by Nessim Halfon on 24/04/2020.
//  Copyright © 2020 Nessim Halfon. All rights reserved.
//

import UIKit
import Charts
import NaturalLanguage

class PieChartResultController: UIViewController {

    @IBOutlet weak var pieChartView: PieChartView!
    
    var text: String?
    
    var traits: [String] = []
    var percentages: [Double] = []
    var array1: [Double] = [100]
    var array2: [Double] = [50,50]
    var array3: [Double] = [33,33,33]
    var array4: [Double] = [25,25,25,25]
    var array5: [Double] = [20,20,20,20,20]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        predict(text)
        
        customizeChart(labels: traits, values: percentages.map{ Double($0) })
        pieChartView.animate(xAxisDuration: 2.0)
        
        
    }
    
    
    // MARK: - Func
    private func predict(_ input: String?) {
        guard let input = input else { return }
        
        if let sentimentPredictor = try? NLModel(mlModel: EXT().model),
            let result = sentimentPredictor.predictedLabel(for: input) {
                addArray(from: result, for: Personality.Ext.rawValue)
        }
        
        if let sentimentPredictor = try? NLModel(mlModel: OPN().model),
            let result = sentimentPredictor.predictedLabel(for: input) {
                addArray(from: result, for: Personality.Opn.rawValue)
        }
        
        if let sentimentPredictor = try? NLModel(mlModel: NEU().model),
            let result = sentimentPredictor.predictedLabel(for: input) {
                addArray(from: result, for: Personality.Neu.rawValue)
        }
        
        if let sentimentPredictor = try? NLModel(mlModel: AGR().model),
            let result = sentimentPredictor.predictedLabel(for: input) {
                addArray(from: result, for: Personality.Agr.rawValue)
        }
        
        if let sentimentPredictor = try? NLModel(mlModel: CON().model),
            let result = sentimentPredictor.predictedLabel(for: input) {
                addArray(from: result, for: Personality.Con.rawValue)
        }
        
        createPercentageArray()
    }
    
    private func addArray(from predict: String, for name: String) {
        switch predict {
            case "y": traits.append(name)
            case "n": print("non")
            default: break
        }
    }
    
    private func createPercentageArray() {
        for _ in 0..<traits.count {
            
            switch traits.count {
            case 1:
                percentages = array1
            case 2:
               percentages = array2
            case 3:
              percentages = array3
            case 4:
               percentages = array4
            case 5:
                percentages = array5
            default:
                break
            }
        }
    }
    
    
    func customizeChart(labels: [String], values: [Double]) {
      
      // 1. Set ChartDataEntry
      var dataEntries: [ChartDataEntry] = []
      for i in 0..<labels.count {
        let dataEntry = PieChartDataEntry(value: values[i], label: labels[i], data:  labels[i] as AnyObject)
        dataEntries.append(dataEntry)
      }
        
      
      // 2. Set ChartDataSet
        let pieChartDataSet = PieChartDataSet(entries: dataEntries, label: nil)
      pieChartDataSet.colors = colorsOfCharts(numbersOfColor: labels.count)
      
      // 3. Set ChartData
      let pieChartData = PieChartData(dataSet: pieChartDataSet)
      let format = NumberFormatter()
        format.numberStyle = .none
      let formatter = DefaultValueFormatter(formatter: format)
      pieChartData.setValueFormatter(formatter)
      
      // 4. Assign it to the chart's data
      pieChartView.data = pieChartData
    }
    
    private func colorsOfCharts(numbersOfColor: Int) -> [UIColor] {
      var colors: [UIColor] = []
      for _ in 0..<numbersOfColor {
        let red = Double(arc4random_uniform(256))
        let green = Double(arc4random_uniform(256))
        let blue = Double(arc4random_uniform(256))
        let color = UIColor(red: CGFloat(red/255), green: CGFloat(green/255), blue: CGFloat(blue/255), alpha: 1)
        colors.append(color)
      }
      return colors
    }
    
    private func displayInfo() -> String {
        let ext = "Extravagant: Enthousiame, Sociable, Bizarre, Hors nome..."
        let opn = "Ouvert: Imagination, Curieux, Réceptif, Sensible, Tolérant, Compréhension..."
        let neu = "Névrotisme: Ancieux, Colére, Culpabilité, Déprime, Phobie, Peur, Dépression..."
        let agr = "Agréable: Compassion, Généreux, Coopératif, Prend soin des autres..."
        let con = "Consciencieux: Organisé, Discipliné, Sens moral, Respest des régles..."
        
        var message = " "
        let jump = "\n"
        
        for trait in traits {
            switch trait {
            case Personality.Ext.rawValue: message += ext + jump
            case Personality.Opn.rawValue: message += opn + jump
            case Personality.Neu.rawValue: message += neu + jump
            case Personality.Agr.rawValue: message += agr + jump
            case Personality.Con.rawValue: message += con + jump
            default: break
            }
        }
        
        return message
    }
    
    
    //MARK:- IBAction
    
    @IBAction func infoButton(_ sender: UIBarButtonItem) {
        displayAlert(title: "Définition", message: displayInfo())
    }
    

}
