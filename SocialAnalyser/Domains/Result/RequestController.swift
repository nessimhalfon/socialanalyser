//
//  RequestController.swift
//  SentimentAnalyser
//
//  Created by Nessim Halfon on 26/04/2020.
//  Copyright © 2020 Nessim Halfon. All rights reserved.
//

import UIKit
import NaturalLanguage

class RequestController: UIViewController {

    //MARK:- IBOutlet
    @IBOutlet weak var loadingLabel: UILabel!
    
    //MARK:- Properties
    var text: String?
    var username: String?
    
    var traitsPlus: [String] = []
    var percentagesPlus: [Double] = []
    
    var traitsMoins: [String] = []
    var percentagesMoins: [Double] = []
    
    var array1: [Double] = [100]
    var array2: [Double] = [50,50]
    var array3: [Double] = [33,33,33]
    var array4: [Double] = [25,25,25,25]
    var array5: [Double] = [20,20,20,20,20]
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.setHidesBackButton(true, animated:true)
        predict(text)
    }
    
    //MARK:- Design Func
    
    /// Permet d'afficher l'écran de chargement.
    private func setupLoadingLabel() {
        if let username = username {
            loadingLabel.attributedText = changeAttribute(forString: "Veuillez patienter pendant l'analyse de la personnalité de \(username) ...", values: [username], color: UIColor.MyTheme.bordeau)
            if username == "me" {
                loadingLabel.text = "Veuillez patienter pendant l'analyse de votre personnalité ..."
            }
        } else {
            loadingLabel.text = "Veuillez patienter pendant l'analyse de votre texte ..."
        }
       
    }
    
    /// Permet de remplir les array pour les traits (Plus/Moins).
    /// - parameter predict : La valeur de la prédiction.
    /// - parameter name : Le nom du trait de la personnalité.
    private func createPieChartLabel(from predict: String, for name: String) {
        switch predict {
            case "y": traitsPlus.append(name)
            case "n": traitsMoins.append(name)
            default: break
        }
    }
    
    /// PErmet de remplir les valeurs de chaque traits avec un %tage.
    /// - parameter labels : Les labels créer en fonction de l'analyse.
    /// Return un array avec les valeurs.
    private func createPieChartValues(labels: [String]) -> [Double] {
        var values: [Double] = []
        for _ in 0..<labels.count {
            switch labels.count {
            case 1:
                values = array1
            case 2:
                values = array2
            case 3:
                values = array3
            case 4:
                values = array4
            case 5:
                values = array5
            default:
                break
            }
        }
        return values
    }
    
    /// Permet de créer les arrays pour les %tage.
    private func createPieChartValues() {
        percentagesPlus = createPieChartValues(labels: traitsPlus)
        percentagesMoins = createPieChartValues(labels: traitsMoins)
    }
    
    
    //MARK:- ML Func
    
    /// Permet de faire la prédiction en fonction d'un text.
    /// - parameter input : Le text à prédire.
    private func predict(_ input: String?) {
        guard let input = input else { return }
        
        if let sentimentPredictor = try? NLModel(mlModel: EXT().model),
            let result = sentimentPredictor.predictedLabel(for: input) {
                createPieChartLabel(from: result, for: Personality.Ext.rawValue)
        }
        
        if let sentimentPredictor = try? NLModel(mlModel: OPN().model),
            let result = sentimentPredictor.predictedLabel(for: input) {
                createPieChartLabel(from: result, for: Personality.Opn.rawValue)
        }
        
        if let sentimentPredictor = try? NLModel(mlModel: NEU().model),
            let result = sentimentPredictor.predictedLabel(for: input) {
                createPieChartLabel(from: result, for: Personality.Neu.rawValue)
        }
        
        if let sentimentPredictor = try? NLModel(mlModel: AGR().model),
            let result = sentimentPredictor.predictedLabel(for: input) {
                createPieChartLabel(from: result, for: Personality.Agr.rawValue)
        }
        
        if let sentimentPredictor = try? NLModel(mlModel: CON().model),
            let result = sentimentPredictor.predictedLabel(for: input) {
                createPieChartLabel(from: result, for: Personality.Con.rawValue)
        }

        createPieChartValues()
        setupLoadingLabel()
        goToResult()
    }
    
    /// Permet de se diriger vers la ResultController.
    private func goToResult() {
        guard let result = storyboardViewController(name: .Result, identifier: .PieChartResult) as? PieChartResultController else { return }
        result.text = text
        result.traitsPlus = traitsPlus
        result.percentagesPlus = percentagesPlus
        
        result.traitsMoins = traitsMoins
        result.percentagesMoins = percentagesMoins
        result.username = self.username
        
        showLoader()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.hideLoader(view: self.view)
            self.navigationController?.pushViewController(result, animated: true)
        }
    }
}
