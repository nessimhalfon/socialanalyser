//
//  PieChartResultController.swift
//  SentimentAnalyser
//
//  Created by Nessim Halfon on 24/04/2020.
//  Copyright © 2020 Nessim Halfon. All rights reserved.
//

import UIKit
import Charts

class PieChartResultController: UIViewController {

    //MARK:- IBOutlet
    @IBOutlet weak var pieChartView: PieChartView!
    @IBOutlet weak var predictionLabel: UILabel!
    @IBOutlet weak var styleSegmentedControl: UISegmentedControl!
    
    //MARK:- Properties
    var text: String?
    var username: String?
    
    var traitsPlus: [String]?
    var percentagesPlus: [Double] = []
    var pieChartDataPlus: PieChartData?
    
    var traitsMoins: [String]?
    var percentagesMoins: [Double] = []
    var pieChartDataMoins: PieChartData?
    
    var colors: [UIColor] = []
    var colors2: [UIColor] = []
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setupBackButton()
        createColorArray()
        setupPieChart()
        setupInfo()
        pieChartView.centerText = "Plus"
        
    }
    
    //MARK:- Setup Func
    
    /// Permet de setup les PieChart avec les données.
    private func setupPieChart() {
        pieChartDataPlus = createPieChartData(labels: traitsPlus!, values: percentagesPlus.map{ Double($0) }, colors: colorsOfCharts(numbers: traitsPlus!.count, colors: colors))
        pieChartDataMoins = createPieChartData(labels: traitsMoins!, values: percentagesMoins.map{ Double($0) }, colors: colorsOfCharts(numbers: traitsMoins!.count, colors: colors2))
        pieChartView.data = pieChartDataPlus
        pieChartView.animate(xAxisDuration: 2.0)
    }
    
    /// Permet de créer les arrays avec les couleurs pour les PieCharts.
    private func createColorArray() {
        colors.append(UIColor.Personality.queenBlue)
        colors.append(UIColor.Personality.candyPink)
        colors.append(UIColor.Personality.vegasGold)
        colors.append(UIColor.Personality.forestGreen)
        colors.append(UIColor.Personality.rawSienna)
        
        colors2 = colors.reversed()
    }
    
    /// Permet de setup le BackButton.
    private func setupBackButton() {
        navigationItem.hidesBackButton = true
        
        let newBackButton = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(back(sender:)))
        navigationItem.leftBarButtonItem = newBackButton
    }
    
    /// Permet de répartir les couleurs en 2 pour les 2 PieCharts.
    /// - parameter numbers : Le nombre de case dans un PieCharts.
    /// - parameter colors : Les couleurs pour les cases d'un PieCharts.
    /// return un array de colors.
    private func colorsOfCharts(numbers: Int, colors: [UIColor]) -> [UIColor] {
        var colorsOutput: [UIColor] = []
        
        for i in 0..<numbers {
            colorsOutput.append(colors[i])
        }
        return colorsOutput
    }

    /// Permet de créer les PieChart.
    /// - parameter labels : Les labels du PieChart.
    /// - parameter values : Les valeurs en %tage de chaque case du PieChart.
    /// - parameter colors : Les couleurs de chaque case du PieChart.
    /// return le PieChart créer.
    func createPieChartData(labels: [String], values: [Double], colors: [UIColor]) -> PieChartData{
      
        // 1. Set ChartDataEntry
        var dataEntries: [ChartDataEntry] = []
        for i in 0..<labels.count {
            let dataEntry = PieChartDataEntry(value: values[i], label: labels[i], data:  labels[i] as AnyObject)
            dataEntries.append(dataEntry)
        }
            
        // 2. Set ChartDataSet
        let pieChartDataSet = PieChartDataSet(entries: dataEntries, label: nil)
        pieChartDataSet.colors = colors
          
        // 3. Set ChartData
        let pieChartData = PieChartData(dataSet: pieChartDataSet)
        let format = NumberFormatter()
        format.numberStyle = .none
        let formatter = DefaultValueFormatter(formatter: format)
        pieChartData.setValueFormatter(formatter)
          
        return pieChartData
    }
    
    /// Permet d'afficher les info de l'User/Text analysé.
    private func setupInfo() {
        if let username = username {
            predictionLabel.attributedText = changeAttribute(forString: "Nous avons prédit que les traits de la personnalité de \(username) sont : ", values: [username], color: UIColor.MyTheme.bordeau)
            if username == "me" {
                predictionLabel.text = "Nous avons prédit que vos traits de la personnalité sont : "
            }
        } else {
            predictionLabel.text = "Nous avons prédit que les traits de la personnalité d'après votre texte sont :"
        }
    }
    
    //MARK:- IBAction
    
    /// Permet de revenir 2 Controller en Arriere (Pour éviter d'arrivé sur le RequestController)
    /// - parameter sender : Le button dans la NavigationBar.
    @objc func back(sender: UIBarButtonItem) {
        self.popBack(3)
    }
    
    /// Permet de revenir n Controller en Arriere (Pour éviter d'arrivé sur le RequestController)
    /// - parameter nb : Le nombre de Controller à sauter.
    @objc func popBack(_ nb: Int) {
        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            guard viewControllers.count < nb else {
                self.navigationController?.popToViewController(viewControllers[viewControllers.count - nb], animated: true)
                return
            }
        }
    }
    
    /// Action permettant de changé d'index pour avoir les PieCharts Plus/Moins.
    @IBAction func styleIndexChangedValue(_ sender: UISegmentedControl) {
        switch styleSegmentedControl.selectedSegmentIndex {
        case 0:
            pieChartView.data = pieChartDataPlus
            pieChartView.centerText = "Plus"
        case 1:
            pieChartView.data = pieChartDataMoins
            pieChartView.centerText = "Moins"
        default: break
        }
    }
}

//MARK:-
extension PieChartResultController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailChart",
            let controller = segue.destination as? ResultController {
            controller.text = self.text
            controller.username = self.username
        }
    }
}
