//
//  ResultController.swift
//  SentimentAnalyser
//
//  Created by Nessim Halfon on 07/04/2020.
//  Copyright © 2020 Nessim Halfon. All rights reserved.
//

import UIKit
import NaturalLanguage

class ResultController: UIViewController {

    // MARK:- IBOutlet
    @IBOutlet weak var predictionLabel: UILabel!
    @IBOutlet weak var extImageView: UIImageView!
    @IBOutlet weak var opnImageView: UIImageView!
    @IBOutlet weak var neuImageView: UIImageView!
    @IBOutlet weak var agrImageView: UIImageView!
    @IBOutlet weak var conImageView: UIImageView!
    
    // MARK:- Properties
    var text: String?
    var username: String?
    
    // MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        predict(text)
        setupInfo()
    }
    
    
    // MARK:- Func
    
    /// Permet de faire la prédiction en fonction d'un text.
    /// - parameter input : Le text à prédire.
    private func predict(_ input: String?) {
        guard let input = input else { return }
        
        if let sentimentPredictor = try? NLModel(mlModel: EXT().model),
            let result = sentimentPredictor.predictedLabel(for: input) {
            changeImage(from: result, for: extImageView)
        }
        
        if let sentimentPredictor = try? NLModel(mlModel: OPN().model),
            let result = sentimentPredictor.predictedLabel(for: input) {
             changeImage(from: result, for: opnImageView)
        }
        
        if let sentimentPredictor = try? NLModel(mlModel: NEU().model),
            let result = sentimentPredictor.predictedLabel(for: input) {
             changeImage(from: result, for: neuImageView)
        }
        
        if let sentimentPredictor = try? NLModel(mlModel: AGR().model),
            let result = sentimentPredictor.predictedLabel(for: input) {
             changeImage(from: result, for: agrImageView)
        }
        
        if let sentimentPredictor = try? NLModel(mlModel: CON().model),
            let result = sentimentPredictor.predictedLabel(for: input) {
             changeImage(from: result, for: conImageView)
        }
    }
    
    /// Permet de changer l'image en fonction de la prédiction.
    /// - parameter predict : Le resultat de la prédiction.
    /// - parameter imageView : L'ImageView à changer.
    private func changeImage(from predict: String, for imageView: UIImageView) {
        switch predict {
            case "y": imageView.image = UIImage(named: "plus2")?.withTintColor(UIColor.Sentiment.positif)
            case "n": imageView.image = UIImage(named: "moins2")?.withTintColor(UIColor.Sentiment.negatif)
            default: break
        }
    }
    
    /// Permet d'afficher les info de l'User/Text analysé.
    private func setupInfo() {
        if let username = username {
            predictionLabel.attributedText = changeAttribute(forString: "Nous avons prédit que les traits de la personnalité de \(username) sont : ", values: [username], color: UIColor.MyTheme.bordeau)
            if username == "me" {
                predictionLabel.text = "Nous avons prédit que vos traits de la personnalité sont : "
            }
        } else {
            predictionLabel.text = "Nous avons prédit que les traits de la personnalité d'après votre texte sont :"
        }
    }
}

  

