//
//  CustomNav.swift
//  SentimentAnalyser
//
//  Created by Nessim Halfon on 05/05/2020.
//  Copyright © 2020 Nessim Halfon. All rights reserved.
//

import UIKit

class CustomNav: UINavigationController {

    //MARK:- Properties
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }
    
    //MARK:-
    
    /// Setup le design du NavigationController.
    private func setup() {
        navigationBar.barTintColor = UIColor.MyTheme.greyBackground
        navigationBar.isTranslucent = false
        navigationBar.barStyle = .black
        
        navigationBar.titleTextAttributes = [
            .foregroundColor: UIColor.black,
            .font: UIFont(name: "Futura", size: 20)!
        ]
        
        navigationBar.tintColor = UIColor.MyTheme.bordeau
    }
}
