//
//  CustomButton.swift
//  SentimentAnalyser
//
//  Created by Nessim Halfon on 27/04/2020.
//  Copyright © 2020 Nessim Halfon. All rights reserved.
//

import UIKit

class CustomButton: UIButton {

    //MARK:- Init
     override init(frame: CGRect) {
         super.init(frame: frame)
         setup()
     }
    
     required init?(coder aDecoder: NSCoder) {
         super.init(coder: aDecoder)
         setup()
     }

     //MARK:- Func
    
    /// Setup le design du button.
    func setup() {
        layer.cornerRadius = 10
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: -4, height: 4)
        layer.shadowRadius = 4
        layer.shadowOpacity = 0.65
        backgroundColor = #colorLiteral(red: 0.753657043, green: 0, blue: 0.2704032063, alpha: 1)
    }
    
    /// Anime un button avec un effet `pulse`.
    func pulse() {
        let pulse = CASpringAnimation(keyPath: "transform.scale")
        pulse.duration = 0.5
        pulse.fromValue = 0.95
        pulse.toValue = 1
        pulse.autoreverses = true
        pulse.repeatCount = 1
        pulse.initialVelocity = 0.5
        pulse.damping = 1.0
        
        layer.add(pulse, forKey: nil)
    }
}
