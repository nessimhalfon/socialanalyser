//
//  SemiCircleView.swift
//  SentimentAnalyser
//
//  Created by Nessim Halfon on 05/05/2020.
//  Copyright © 2020 Nessim Halfon. All rights reserved.
//

import UIKit

class SemiCircleView: UIView {

    //MARK:- Properties
    var semiCirleLayer: CAShapeLayer!

    //MARK:- Func
    override func layoutSubviews() {
        super.layoutSubviews()

        setup()
    }
    
    /// Setup le design de la view.
    private func setup() {
        if semiCirleLayer == nil {
            let arcCenter = CGPoint(x: bounds.size.width / 2, y: bounds.size.height / 2)
            let circleRadius = bounds.size.width / 2
            let circlePath = UIBezierPath(arcCenter: arcCenter, radius: circleRadius, startAngle: 2 * CGFloat.pi, endAngle: CGFloat.pi, clockwise: true)

            semiCirleLayer = CAShapeLayer()
            semiCirleLayer.path = circlePath.cgPath
            semiCirleLayer.fillColor = #colorLiteral(red: 0.7616397738, green: 0, blue: 0.2618119121, alpha: 1)
            layer.addSublayer(semiCirleLayer)

            backgroundColor = UIColor.clear
        }
    }
}
