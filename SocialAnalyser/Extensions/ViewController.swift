//
//  ViewController.swift
//  SentimentAnalyser
//
//  Created by Nessim Halfon on 16/12/2019.
//  Copyright © 2019 Nessim Halfon. All rights reserved.
//

import UIKit
import MBProgressHUD

var vSpinner : UIView?

extension UIViewController {
    
    /// Permet d'afficher un Controller en l'instantiant par le storyboard.
    /// - parameter name : Le nom du file du Storyboard.
    /// - parameter identifier : Le nom du Controller.
    func storyboardViewController(name: Storyboard, identifier: Controller) -> UIViewController {
        return UIStoryboard(name: name.rawValue, bundle: nil).instantiateViewController(withIdentifier: identifier.rawValue)
    }
    
    /// Permet de se déplacer au Controller.
    /// - parameter name : Le nom du Storyboard.
    /// - parameter identifier : Le nom du Controller.
    func show(_ name: Storyboard, identifier: Controller) {
        let vc = storyboardViewController(name: name, identifier: identifier)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    /// Permet de cacher le clavier.
    @objc func hideKeybord() {
        view.endEditing(true)
    }
    
    /// Permet d'afficher une alerte.
    /// - parameter title : Le titre de l'alerte.
    /// - parameter message : Le message de l'alerte.
    /// - parameter completion : Permet d'avoir une fonction aprés l'action.
    /// - parameter handler : Permet d'avoir une action.
    func displayAlert(title: String?, message: String?, completion: (() -> Void)? = nil, handler: ((UIAlertAction) -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: handler))
        present(alert, animated: true, completion: completion)
    }
    
    
    /// Change la couleur de plusieurs parties d'une string.
    /// - parameter string : Le string qu'on veux modifier.
    /// - parameter values : Le morceau du String à changer.
    /// - parameter color : La couleur du morceau du String à changer.
    /// return un AttributedString changé.
    func changeAttribute(forString string: String, values: [String], color: UIColor) -> NSMutableAttributedString  {
        let attributedString = NSMutableAttributedString(string: string)
        for value in values {
            attributedString.setColorForText(textForAttribute: value, withColor: color)
            attributedString.setBoldForText(textForAttribute: value)
        }
        return attributedString
    }
    
    /// Permet d'afficher le Loader.
    func showLoader() {
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = "Chargement..."
    }
    
    /// Permet de cacher le Loader.
    func hideLoader(view: UIView) {
        MBProgressHUD.hide(for: view, animated: true)
    }
}
