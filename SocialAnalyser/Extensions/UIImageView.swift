//
//  UIImageView.swift
//  SentimentAnalyser
//
//  Created by Nessim Halfon on 23/04/2020.
//  Copyright © 2020 Nessim Halfon. All rights reserved.
//

import UIKit
import SDWebImage

extension UIImageView {
    
    /// Permet de télécharger une image en fonction d'une URL.
    /// - parameter string : L'url de l'image.
    func download(string: String?) {
        guard let string = string else { return }
        sd_setImage(with: URL(string: string), placeholderImage: UIImage(named: "stars"), options: .highPriority, completed: nil)
    }
}
