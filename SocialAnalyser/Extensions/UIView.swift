//
//  UIView.swift
//  SentimentAnalyser
//
//  Created by Nessim Halfon on 05/05/2020.
//  Copyright © 2020 Nessim Halfon. All rights reserved.
//

import UIKit

extension UIView {
    
    /// Permet de créer un shadow à une UIView.
    func setShadow() {
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 1)
        self.layer.shadowOpacity = 0.2
        self.layer.shadowRadius = 6.0
        self.layer.masksToBounds = false
        self.layer.cornerRadius = 14
        self.clipsToBounds = false
    }

    /// Permet de créer un bord arrondi et une couleur à une UIView.
    /// - parameter color : La couleur du contour.
    /// - parameter borderWidth : La taille du contour.
    func setRoundCorner(color: UIColor, borderWidth: CGFloat = 3.0) {
        self.layer.cornerRadius = 14
        self.layer.masksToBounds = true
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = color.cgColor
        self.backgroundColor = .white
    }
}
