//
//  Lemmatize.swift
//  SentimentAnalyser
//
//  Created by Nessim Halfon on 06/04/2020.
//  Copyright © 2020 Nessim Halfon. All rights reserved.
//

import UIKit

extension String {
    
    //MARK:- Properties
    
    /// Permet d'avoir la taille d'une image de Twitter.
    var imageFullSize: String {
        return self.replacingOccurrences(of: "_normal", with: "")
    }
    
    /// Permet de retirer les #.
    var textWitouthHashTag: String {
        return self.replacingOccurrences(of: "#", with: "")
    }
       
    /// Permet de retirer les @.
    var textWithoutAt: String {
        return self.replacingOccurrences(of: "@", with: "")
    }
    
    /// Permet de retirer les RT.
    var textWithoutRT: String {
        return self.replacingOccurrences(of: "RT ", with: "")
    }
    
    //MARK:- Func
    
    /// Permet de netoyer un String en retirant : #, @, RT, Email.
    func lematize() -> String {

        let textWithoutHashtag = self.textWitouthHashTag
        let textWithoutAt = textWithoutHashtag.textWithoutAt
        let text = textWithoutAt.textWithoutRT
        
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        let matches = detector.matches(in: text, options: [], range: NSRange(location: 0, length: text.utf16.count))

        for match in matches {
            guard let range = Range(match.range, in: text) else { continue }
            let url = text[range]
            
            return text.replacingOccurrences(of: url, with: "")
        }
        return text
    }
}

extension NSMutableAttributedString {

    /// Permet de mettre une couleur sur un string.
    /// - parameter textForAttribute : Le string à modifier.
    /// - parameter color : La couleur.
    func setColorForText(textForAttribute: String, withColor color: UIColor) {
        let range: NSRange = self.mutableString.range(of: textForAttribute, options: .caseInsensitive)
        self.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
    }
    
    /// Permet de mettre un text en gras.
    /// - parameter textForAttribute : Le text à mettre en gras.
    func setBoldForText(textForAttribute: String) {
        let range: NSRange = self.mutableString.range(of: textForAttribute, options: .caseInsensitive)
        self.addAttribute(.font, value: UIFont.boldSystemFont(ofSize: 20.0), range: range)
    }
}
