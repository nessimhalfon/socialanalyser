//
//  Date.swift
//  SentimentAnalyser
//
//  Created by Nessim Halfon on 05/05/2020.
//  Copyright © 2020 Nessim Halfon. All rights reserved.
//

import UIKit

//MARK:-
extension Formatter {
    
    /// Le Format de la date de Twitter.
    static let twitterDate : DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "EEE MMM dd HH:mm:ss Z yyyy"
        
        return formatter
    }()
    
    /// Le Format de la data à afficher.
    static let displayDate: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMMM yyyy"
        formatter.locale = Locale(identifier: "fr_FR")
        return formatter
    }()
}

//MARK:-
extension String {
    
    /// Transforme un String en Date.
    var twitterDate: Date? {
        return Formatter.twitterDate.date(from: self)
    }
    
    /// Permet d'afficher la date en String.
    func parseTwitterDate() -> String {
        guard let date = twitterDate else { return "" }
        return Formatter.displayDate.string(from: date)
    }
    
}

