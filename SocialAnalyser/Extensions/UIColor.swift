//
//  UIColor.swift
//  SentimentAnalyser
//
//  Created by Nessim Halfon on 24/04/2020.
//  Copyright © 2020 Nessim Halfon. All rights reserved.
//

import UIKit

extension UIColor {
    
    /// Permet de créer une couleur RGB plus simplement.
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    /// Permet d ecréer une couleur hexa.
    convenience init(netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
    
    //MARK:- Personality Color
    struct Personality {
        static let queenBlue = UIColor(red: 69, green: 105, blue: 144)
        static let candyPink =  UIColor(red: 239, green: 118, blue: 122)
        static let vegasGold = UIColor(red: 185, green: 164, blue: 76)
        static let forestGreen = UIColor(red: 81, green: 152, blue: 114)
        static let rawSienna = UIColor(red: 202, green: 137, blue: 95)
    }
    
    //MARK:- Sentiment Color
    struct Sentiment {
        static let positif = UIColor(red: 128, green: 238, blue: 30)
        static let negatif = UIColor(red: 245, green: 3, blue: 3)
    }
    
    //MARK:- Design Color
    struct MyTheme {
        static let greyBackground = UIColor(red: 239, green: 239, blue: 239)
        static let bordeau = #colorLiteral(red: 0.753657043, green: 0, blue: 0.2704032063, alpha: 1)
    }
}

