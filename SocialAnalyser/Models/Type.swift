//
//  Type.swift
//  SentimentAnalyser
//
//  Created by Nessim Halfon on 15/12/2019.
//  Copyright © 2019 Nessim Halfon. All rights reserved.
//

import Foundation

//MARK:- ML Response
enum Type: String {
    case Negative
    case Positive
    case Neutral
}

//MARK:- Storyboard ID
enum Controller: String {
    case Home = "HomeController"
    case SearchTwitter = "SearchTwitterController"
    case Tweets = "TweetsController"
    case Text = "TextController"
    case MyTweets = "MyTweetsController"
    case VIP = "VIPController"
    case Request = "RequestController"
    case PieChartResult = "PieChartResultController"
    case Result = "ResultController"
}

enum Storyboard: String {
    case Home = "Home"
    case SearchTwitter = "SearchTwitter"
    case Text = "Text"
    case MyTweets = "MyTweets"
    case VIP = "VIP"
    case Result = "Result"
    
}

//MARK:- Personality Traits
enum Personality: String {
    case Ext = "Extravagant"
    case Opn = "Ouvert"
    case Neu = "Nerveux"
    case Agr = "Agréable"
    case Con = "Consciencieux"
}
