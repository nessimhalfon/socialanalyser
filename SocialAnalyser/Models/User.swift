//
//  User.swift
//  SentimentAnalyser
//
//  Created by Nessim Halfon on 08/04/2020.
//  Copyright © 2020 Nessim Halfon. All rights reserved.
//

import Foundation

class User {
    
    //MARK:- Properties
    private var _id: String
    private var _screenname: String
    private var _username: String
    private var _imageUrl: String?
    private var _isLog: Bool? = false
    private var _created: String
    
    //MARK:- Init
    init(id: String?, screenname: String?, isLog: Bool?, username: String?, imageUrl: String?, created: String) {
        self._id = id ?? "00"
        self._screenname = screenname ?? "JonDoe"
        self._isLog = isLog
        self._username = username ?? "Jon Doe"
        self._imageUrl = imageUrl ?? ""
        self._created = created
    }
    
    
    //MARK:- Getter/Setter
    
    var id: String {
        get {
            return _id
        }
        set {
            _id = newValue
        }
    }
    
    var screenname: String {
        get {
            return _screenname
        }
        set {
            _screenname = newValue
        }
    }
    
    var username: String {
        get {
            return _username
        }
        set {
            _username = newValue
        }
    }
    
    var imageUrl: String? {
        get {
            return _imageUrl
        }
        set {
            _imageUrl = newValue ?? ""
        }
    }

    var isLog: Bool? {
        get {
            return _isLog ?? false
        }
        set {
            _isLog = newValue
        }
    }
    
    var created: String {
        get {
            return _created
        }
        set {
            _created = newValue
        }
    }
    
    //MARK:- Static Func
    
    /// Permet de créer un array de personne connu qu'on va utilisé pour notre view VIPController.
    static func createVIP() -> [User] {
        var vips: [User] = []
        let trump = User(id: nil, screenname: "realDonaldTrump", isLog: nil, username: "Donald Trump", imageUrl: "trump", created: "")
        let macron = User(id: nil, screenname: "EmmanuelMacron", isLog: nil, username: "Emmanuel Macron", imageUrl: "macron", created: "")
        let cook = User(id: nil, screenname: "tim_cook", isLog: nil, username: "Tim Cook", imageUrl: "cook", created: "")
        let musk = User(id: nil, screenname: "elonmusk", isLog: nil, username: "Elon Musk", imageUrl: "musk", created: "")
        let gates = User(id: nil, screenname: "_bilgates_", isLog: nil, username: "Bil Gates", imageUrl: "gates", created: "")
        let obama = User(id: nil, screenname: "BarackObama", isLog: nil, username: "Barack Obama", imageUrl: "obama", created: "")
        let lattner = User(id: nil, screenname: "clattner_llvm", isLog: nil, username: "Chris Lattner", imageUrl: "lattner", created: "")

        vips.append(contentsOf: [trump, cook, lattner, musk, obama, gates, macron])
        
        return vips
    }
    
    /// Permet de créer un array de notre équipe de développement qu'on va utilisé pour notre view VIPController.
    static func createTeam() -> [User] {
        var teams: [User] = []
        let nessim = User(id: nil, screenname: "HALFONNessim", isLog: nil, username: "Nessim Halfon", imageUrl: "nessim", created: "")
        let jordan = User(id: nil, screenname: "Jordan_Chicha", isLog: nil, username: "Jordan Chicha", imageUrl: "jordan", created: "")
        let avi = User(id: nil, screenname: "avifitouss", isLog: nil, username: "Avi Fitoussi", imageUrl: "avi", created: "")
        let ethan = User(id: nil, screenname: "EthanSitbon", isLog: nil, username: "Ethan Sitbon", imageUrl: "ethan", created: "")
        
        teams.append(contentsOf: [nessim, jordan, avi, ethan])
        
        return teams
    }
    
    /// Permet de créer un array d'entreprise qu'on va utilisé pour notre view VIPController.
    static func createCompany() -> [User] {
        var companys: [User] = []
        let apple = User(id: nil, screenname: "Apple", isLog: nil, username: "Apple", imageUrl: "apple", created: "")
        let amazon = User(id: nil, screenname: "Amazon", isLog: nil, username: "Amazon", imageUrl: "amazon", created: "")
        let tesla = User(id: nil, screenname: "Tesla", isLog: nil, username: "Tesla", imageUrl: "tesla", created: "")
        let disney = User(id: nil, screenname: "Disney", isLog: nil, username: "Disney", imageUrl: "disney", created: "")
        let microsoft = User(id: nil, screenname: "Microsoft", isLog: nil, username: "Microsoft", imageUrl: "microsoft", created: "")
        let uber = User(id: nil, screenname: "Uber", isLog: nil, username: "Uber", imageUrl: "uber", created: "")
        
        companys.append(contentsOf: [apple, amazon, tesla, disney ,microsoft, uber])

        return companys
    }
}


