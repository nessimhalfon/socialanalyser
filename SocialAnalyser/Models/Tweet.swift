//
//  Tweet.swift
//  SentimentAnalyser
//
//  Created by Nessim Halfon on 16/12/2019.
//  Copyright © 2019 Nessim Halfon. All rights reserved.
//

import UIKit

struct Tweet {
    
    //MARK:- Properties
    let text: String
    let sentiment: String
    let color: UIColor
    
    //MARK:- Init
    init(text: String, sentiment: String, color: UIColor) {
        self.text = text
        self.sentiment = sentiment
        self.color = color
    }
}
