//
//  Service.swift
//  SentimentAnalyser
//
//  Created by Nessim Halfon on 16/12/2019.
//  Copyright © 2019 Nessim Halfon. All rights reserved.
//

import UIKit
import SwifteriOS

class Service {
    
    //MARK:- Properties
    private let url = URL(string: "swifter://success")!
    private let swifter = Swifter(consumerKey: SecretAPI.consumerKey.rawValue, consumerSecret: SecretAPI.consumerSecret.rawValue)
    
    //MARK:- Func
    
    /// Permet de récupéré les twitters en fontion de la recherche.
    /// - parameter text : La recherche des tweets.
    /// - parameter lang : La langue des tweets.
    /// - parameter count : Le nombre de tweets.
    /// - parameter completion : retourne un JSON asynchronement.
    func fetch(text: String, lang: String? ,count: Int, completion: @escaping ServiceResultCompletion<JSON>) {
        swifter.searchTweet(using: text, lang: lang, count: count, tweetMode: .extended, success: { (result, metadata) in
            completion(ServiceResult.success(value: result))
        }) { (error) in
            completion(ServiceResult.failure(error: ServiceError.message(error.localizedDescription)))
        }
    }
    
    /// Permet de récupéré les tweets d'une personne en particulier.
    /// - parameter name : La personne.
    /// - parameter completion : retourne un JSON asynchronement.
    func fetchTimeLine(for name : String, completion: @escaping ServiceResultCompletion<[JSON]>) {
        swifter.getTimeline(for: .screenName(name), count: 50, tweetMode: .extended, success: { json in
            completion(ServiceResult.success(value: json.array ?? []))
        }) { error in
            completion(ServiceResult.failure(error: ServiceError.message(error.localizedDescription)))
        }
    }
    
    /// Permet de se connecter via son compte Twitter.
    /// - parameter vc : Le view controller qui redirigera vers la page de connection.
    /// - parameter completion : retourne un User asynchronement.
    func login(vc: UIViewController, completion: @escaping ServiceResultCompletion<User>) {
        swifter.authorize(withCallback: url, presentingFrom: vc, success: { (auth, _) in
            
            let user = User(id: auth?.userID, screenname: auth?.screenName, isLog: true, username: nil, imageUrl: nil, created: "")
            
            completion(ServiceResult.success(value: user))
        }) { error in
            completion(ServiceResult.failure(error: ServiceError.message(error.localizedDescription)))
        }
    }
    
    /// Permet de récupéré les informations d'une utilisateur.
    /// - parameter name : Les infos de l'utilisateur.
    /// - parameter completion : retourne un JSON asynchronement.
    func fetchUserInfo(_ name: String,  completion: @escaping ServiceResultCompletion<JSON>) {
        swifter.showUser(.screenName(name), success: { json in
            completion(ServiceResult.success(value: json))
        }) { error in
            completion(ServiceResult.failure(error: ServiceError.message(error.localizedDescription)))
        }
    }
}
