//
//  ServiceType.swift
//  SentimentAnalyser
//
//  Created by Nessim Halfon on 16/12/2019.
//  Copyright © 2019 Nessim Halfon. All rights reserved.
//

import Foundation

enum ServiceError: Error {
    case noNetwork
    case serviceWith(code: Int, message: String)
    case message(_ message: String)
    case technicalError
}

enum ServiceResult<T> {
    case success(value: T)
    case failure(error: ServiceError)
}

typealias ServiceResultCompletion<T> = (ServiceResult<T>) -> Void
