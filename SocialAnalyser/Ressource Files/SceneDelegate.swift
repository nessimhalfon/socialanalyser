//
//  SceneDelegate.swift
//  SentimentAnalyser
//
//  Created by Nessim Halfon on 15/12/2019.
//  Copyright © 2019 Nessim Halfon. All rights reserved.
//

import UIKit
import SwifteriOS

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let _ = (scene as? UIWindowScene) else { return }
    }
    
    func scene(_ scene: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>) {
        guard let context = URLContexts.first else { return }

        let callbackUrl = URL(string: "votre lien pour la connexion a Twitter")!
        Swifter.handleOpenURL(context.url, callbackURL: callbackUrl)
    }
}

