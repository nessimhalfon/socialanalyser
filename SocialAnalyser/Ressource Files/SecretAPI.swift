//
//  SecretAPI.swift
//  SentimentAnalyser
//
//  Created by Nessim Halfon on 15/12/2019.
//  Copyright © 2019 Nessim Halfon. All rights reserved.
//

import Foundation

enum SecretAPI: String {
    case consumerKey = "votre consumerKey"
    case consumerSecret = "votre consumerSecret"
}
