# Social Analyser 

## Résumé
Nous envoyons de nombreux messages et photos au fil du temps sur nos réseaux sociaux tels que Facebook, Instagram ou Twitter. Ce phénomène crée un flux de données non structuré. 
Il est intéressant pour une entreprise de savoir comment elle est perçu par ses clients, ce qui peut les aider à mieux évaluer un produit ou une publicité.
D’autre part, Il est intéressant pour la plupart des gens de connaître leur personnalité, ce qui peut les aider à mieux se connaître. En outre, il y a des moments où prédire la personnalité de l'autre dans le cadre du processus d'emploi pourrait conduire à former une meilleure équipe. 
L'idée derrière ce projet est de créer une application mobile qui aide l'utilisateur à prédire sa propre personnalité ou celle des autres et de prédire la popularité d’une entreprise, personne ou technologie afin de savoir l’impact qu’elle a sur le monde. Après avoir recueilli suffisamment de données auprès de l'utilisateur, nous avons appliqué des algorithmes d'analyse des sentiments à ces données et classé le trait de personnalité de l'utilisateur selon le modèle des cinq grands facteurs. Le résultat est ensuite présenté dans une interface conçue et évaluée au cours de trois itérations et d'un processus d'évaluation basé sur les principes de l'interface utilisateur.

## Installation
### Application Mobile iOS

Pour lancer l'application, le Logiciel Xcode est nécessaire et doit être configuré pour votre Mac.

L'application utilise le gestionnaire de dépendance CocoaPods. Lire plus sur CocoaPod ici : [https://cocoapods.org]()

Pour lancer l'application, rendez vous à la racine du dossier, ouvrez votre terminal et suivé les étapes suivante : 

**Installer CocoaPods**

```sh
$ sudo gem install cocoapods
```

**Lancer le fichier pod**

Pour installer les dépendances nécessaire au projet, exécuter la commande suivante dans votre terminal:


```sh
$ pod install
```

**Ouvrir le workspace**

Ouvrir le fichier `SocialAnalyser.xcworkspace` avec le logiciel `Xcode`.

**Modifier certaine variable**

Dans le fichier `SecretAPI` :
Renseignez votre `consumerKey`et votre `consumerSecret`que vous trouverez sur votre profil développeur Twitter : [https://developer.twitter.com]()

Dans le fichier `AppDelegate` :
Modifier la ligne N°21 : remplacer `"swifter://"` par votre lien configuré sur votre profil développeur Twitter.

Dans le fichier `SceneDelegate` : 
Modifier la ligne N°23 : remplacer `"swifter://"` par votre lien configurer sur votre profil développeur Twitter.

Dans le fichier `info.plist`:
Veuillez renseigner votre consumerKey précéder par swifter- ici : 
URL types > Item 1 > URL Schemes > Item 1 > swifter-consumerKey

**Lancer l'application**

Cliquer sur le bouton lancement en haut à gauche de Xcode ou appuyer  sur `⌘ + R` de votre clavier.

## ⚠️ Attention ⚠️

Les modèles pour l'analyse de sentiment et de la personnalité de sont pas disponible sur ce repository public.
Si vous désirez reproduire cette application veuillez créer vos propre modèles.
